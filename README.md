# graphify

Display some stats and graphs of a database entries.

For the full documentation, installation instructions... check [graphify documentation page](https://squeak.eauchat.org/libs/graphify/).

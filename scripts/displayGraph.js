var _ = require("underscore");
var $$ = require("squeak");
var Chart = require("chart.js");
// require("chartjs-plugin-labels");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function ($container, graphOptionsPassed) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              CREATE CANVAS

  var $graphCanvas = $container.canva({
    class: "graphify-canvas",
  });
  var ctx = $graphCanvas[0].getContext("2d");

  //
  //                              MAKE GRAPH OPTIONS

  var defaultGraphOptions = {
    type: "bar",
    data: {
      _recursiveOption: true,
    },
    options: {
      _recursiveOption: true,
      hover: {
        _recursiveOption: true,
        mode: "nearest",
      },

      // LEGEND
      legend: {
        _recursiveOption: true,
        display: false,
      },

      // MAKE CHARTJS RESPECT SIZE OF CONTAINER DIV AND REFRESH ITSELF WHEN CONTAINER IS RESIZED
      responsive: true,
      maintainAspectRatio: false,

      // PLUGINS
      // plugins: {
      //   _recursiveOption: true,
      //   labels: {
      //     render: "value",
      //     fontColor: '#FFF',
      //   },
      // },

      // TOOLTIPS
      // tooltips: {
      //   _recursiveOption: true,
      //   // enabled: false,
      //   callbacks: {
      //     _recursiveOption: true,
      //     label: function (tooltipModel, data) {
      //       return counts[tooltipModel.index];
      //     },
      //   },
      // },

      // FILTER ON LEGEND CLICK???
      // ?clickDatabaseFilter: <function(labelToFilterBy<string>):<regexp|string>> « the return of this function will be used as regexp to filter entries »,
      // onClick: function (event) {
      //
      //   var chart = this;
      //   var bars = chart.getElementAtEvent(event);
      //   var element = bars[0];
      //
      //   if (element) {
      //     var labelToFilterBy = countsAndLabels[element._index][0];
      //     App.filter(_.isFunction(options.clickDatabaseFilter) ? options.clickDatabaseFilter(labelToFilterBy) : labelToFilterBy);
      //   };
      //
      // },


    },
  };

  var finalGraphOptions = $$.defaults(defaultGraphOptions, graphOptionsPassed);

  //
  //                              CREATE GRAPH

  return new Chart(ctx, finalGraphOptions);

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

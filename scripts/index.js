var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");
var uify = require("uify");
var createGraphifyPanel = require("./panel");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (options) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              DEFAULT OPTIONS

  var defaultOptions = {
    statsOverview: true,
  };
  options = $$.defaults(defaultOptions, options);

  //
  //                              MAKE GRAPHIFIED OBJECT

  var graphified = {
    options: options,
    uuid: $$.uuid(),
    collection: options.collection,
    create: function () {

      // create dialog if necessary
      if (!options.$container) graphified.dialog = uify.dialog.fullscreen({
        content: function ($container) { graphified.$container = $container; },
        hideOnClose: true,
        preventHidingOnClick: true,
      })
      else graphified.$container = options.$container;

      // create pannel
      graphified.refresh();

    },
    refresh: function (newCollection) {

      // refresh collection if it has changed
      if (newCollection) graphified.collection = newCollection;

      // change refresh id to graphified
      graphified.refreshId = $$.uuid();

      // destroy previous panel if there is one
      if (graphified.panel) {
        var activeTab = $$.getValue(graphified, "panel.tabs.activeTab.name");
        graphified.panel.destroy();
      };

      // create graphify panel
      createGraphifyPanel(graphified);

      // if one tab was opened, reopen it
      if (activeTab) graphified.panel.tabs.open(activeTab);

    },
    show: function () { graphified.dialog.show(); },
    hide: function () { graphified.dialog.hide(); },
    toggle: function () { graphified.dialog.toggle(); },
    destroy: function () { graphified.dialog.destroy(); },
  };

  //
  //                              INITIALIZE AND RETURN GRAPHIFIED

  graphified.create()
  return graphified;

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

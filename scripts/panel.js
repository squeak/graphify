var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");
var graphMaker = require("./graph");
var keysStatsMaker = require("./keysStats");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: generate graphs panel
  ARGUMENTS: ( graphified )
  RETURN: <void>
*/
module.exports = function (graphified) {

  graphified.panel = uify.panel({
    $container: graphified.$container,
    title: graphified.options.title,
    closePanelButton: true,
    tabs: _.flatten([

      // STATS OVERVIEW TAB
      graphified.options.statsOverview ? [
        {
          name: "Overview",
          content: _.bind(graphMaker, graphified, {
            title: "List of types used in this database",
            type: "pie",
            dataExtraction: function (collection) { return _.countBy(collection, "type"); },
            sortCounts: "value",
          }),
        },
        {
          name: "Keys stats",
          content: _.bind(keysStatsMaker, graphified),
        },
      ] : [],

      // GRAPHS TABS
      _.map(graphified.options.graphs, function (graphOptions) {
        return {
          name: graphOptions.name,
          content: _.bind(graphMaker, graphified, graphOptions),
        };
      }),

    ], true),
    buttons: [
      {
        title: "refresh graphs",
        icomoon: "spinner11",
        invertColor: true,
        key: "r",
        click: function () { graphified.refresh(); },
      },
    ],
  });
  graphified.panel.$el.addClass("graphify");

};

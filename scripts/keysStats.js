var _ = require("underscore");
var $$ = require("squeak");
var displayGraph = require("./displayGraph");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  COLORS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var colors = {
  string: "green",
  number: "#FF0",
  boolean: "#d90909",
  object: "rebeccapurple",
  array: "#00e6e6",
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  LIST KEYS TYPES AND QUANTITIES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function listKeysAndTypes (keysList, entryOrSubentry, keyPrefix) {
  _.each(entryOrSubentry, function (val, key) {

    // get value type and full key from root
    var valType = $$.typeof(val);
    var fullKey = keyPrefix ? keyPrefix +"."+ key : key;

    // add stats
    if (!keysList[fullKey]) keysList[fullKey] = {};
    if (!keysList[fullKey][valType]) keysList[fullKey][valType] = 1
    else keysList[fullKey][valType] += 1;

    // if object, do same for children (maybe activate it only as option, and with protections so it doesn't itterate anything)
    // if (valType == "object") {
    //   listKeysAndTypes(keysList, val, fullKey);
    // };

  });
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function ($container) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  var graphified = this;

  // make keys and values types stats
  var keysList = {};
  _.each(graphified.collection, function (entry) {
    listKeysAndTypes(keysList, entry);
  });

  // // sort keys
  // var keysListArray = _.chain(keysList).map(function (stats, keyName) {
  //   return {
  //     stats: stats,
  //     keyName: keyName,
  //   };
  // }).sortBy(function (statObj) {
  //   return _.reduce(statObj, function (memo, num){ return memo+num; });
  // }).value();

  // list all types used and prepare datasets
  var typesDatasets = _.chain(keysList).map(_.keys).flatten().uniq().map(function (typeName) {
    return {
      label: typeName,
      backgroundColor: colors[typeName] || $$.random.color(1),
      data: [],
    };
  }).value();
  var labels = [];

  // fill datasets and labels
  _.each(keysList, function (stats, keyName) {
    // add key to labels list
    labels.push(keyName);
    // add values to datasets data
    _.each(typesDatasets, function (datasetObj) {
      datasetObj.data.push(stats[datasetObj.label] || 0);
    });
  });

  //
  //                              RETURN GRAPH OPTIONS

  displayGraph($container, {
    type: "horizontalBar",
    data: {
      labels: labels,
      datasets: typesDatasets,
    },
    options: {
      title: {
        display: true,
        text: "List of keys used in all db entries, and their types",
        fontColor: "white",
      },
      tooltips: {
        mode: "index",
        intersect: false,
        filter: function (label) {
          return +label.value;
        },
      },
      responsive: true,
      scales: {
        xAxes: [{ stacked: true, }],
        yAxes: [{ stacked: true, }],
      },
    },
  });

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

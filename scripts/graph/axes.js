var _ = require("underscore");
var $$ = require("squeak");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE ONE AXIS DEFAULT OPTIONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/


/**
  DESCRIPTION: generated default options for an axis
  ARGUMENTS: (
    !axisId <string>,
    !graphOptions,
  )
  RETURN: <{
    id: <string>,
    ... all possible chart.js axis options
  }>
*/
function makeAxis (axisId, graphOptions) {

  // make default axis parameters
  var axis = {
    id: axisId,
    ticks: {
      beginAtZero: true,
      fontColor: "white",
    },
  };

  // remove some parameters if pie or doughnut chart
  if (_.indexOf(["pie", "doughnut", "polarArea"], graphOptions.type) !== -1) delete axis.ticks;

  // add custom options
  axis = $$.defaults(axis, $$.getValue(graphOptions.axesOptions, axisId));

  // return generated axis object
  return axis;

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: make list of appropriate axes for this graph's datasets
  ARGUMENTS: (
    datasets,
    graphOptions
  )
  RETURN: <{
    xAxes: <chartjsAxisOptionsObject[]>,
    yAxes: <chartjsAxisOptionsObject[]>,
  }>
*/
module.exports = function (datasets, graphOptions) {

  var xAxes = {};
  var yAxes = {};

  _.each(datasets, function (dataset) {
    if (!xAxes[dataset.xAxisID]) xAxes[dataset.xAxisID] = makeAxis(dataset.xAxisID, graphOptions);
    if (!yAxes[dataset.yAxisID]) yAxes[dataset.yAxisID] = makeAxis(dataset.yAxisID, graphOptions);
  });

  return {
    xAxes: _.map(xAxes, function (axisOpts) { return axisOpts; }),
    yAxes: _.map(yAxes, function (axisOpts) { return axisOpts; }),
  };

};

var _ = require("underscore");
var $$ = require("squeak");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GROUP COUNTS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function fillGroupCount (obj, key, value) {
  if (!obj[key]) obj[key] = 0;
  obj[key] += value;
};

function groupCounts (countsAndLabels, groupCountsOption) {

  // get list of counts by groups
  var groupedCounts = _.groupBy(countsAndLabels, groupCountsOption);

  // add all counts to make new list of counts
  return _.map(groupedCounts, function (countsInThisGroup, countsGroupLabel) {

    // new count object
    var groupCount = { label: countsGroupLabel, count: {}, };

    // for each count in this grouped count object,
    _.each(countsInThisGroup, function (countObject) {

      // if counts is an object
      if (_.isObject(countObject.count)) _.each(countObject.count, function (countNumber, countDatasetLabel) {
        fillGroupCount(groupCount.count, countDatasetLabel, countNumber)
      });
      // if count is a simple number
      else fillGroupCount(groupCount.count, "default", countObject.count)

    });

    // return new count object
    return groupCount;

  });

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: make count/label pairs and sort and filter list of counts
  ARGUMENTS: (
    !counts <graphify·countAndLabelObject[]>,
    !graphOptions <graphify·graphOptions>,
  )
  RETURN: <graphify·countAndLabelObject[]>
*/
module.exports = function (counts, graphOptions) {

  // clone
  var countsAndLabels = _.clone(counts);

  // group counts
  if (graphOptions.groupCounts) countsAndLabels = groupCounts(countsAndLabels, graphOptions.groupCounts);

  // eventually filter the counts list
  if (graphOptions.filterCounts) countsAndLabels = _.filter(countsAndLabels, graphOptions.filterCounts);

  // sort the list
  if (_.isFunction(graphOptions.sortCounts)) countsAndLabels = _.sortBy(countsAndLabels, graphOptions.sortCounts)
  else if (graphOptions.sortCounts) countsAndLabels = _.sortBy(countsAndLabels, function (countAndLabel) {
    return countAndLabel[graphOptions.sortCounts];
  })
  else countsAndLabels = _.sortBy(countsAndLabels, function (countAndLabel) {
    return _.isObject(countAndLabel.count) ? countAndLabel.count.default : countAndLabel.count;
  });

  // reverse sorting
  if (graphOptions.sortReverse) countsAndLabels.reverse();

  // return list of label and count pairs
  return countsAndLabels;

};

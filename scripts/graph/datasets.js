var _ = require("underscore");
var $$ = require("squeak");
var pattern = require("patternomaly");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  PATTERNOMALIFY
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var possiblePatterns = [
  "plus", "cross", "dash", "cross-dash", "dot", "dot-dash", "disc", "ring",
  "line", "line-vertical", "weave", "zigzag", "zigzag-vertical",
  "diagonal", "diagonal-right-left", "square", "box",
  "triangle", "triangle-inverted", "diamond", "diamond-box",
];
function patternomalify (color) {
  return pattern.draw($$.random.entry(possiblePatterns), color);
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE DATASETS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
    generate list of all datasets that are relevant for the given counts data
    will look into all counts to see which dataset they expect
  ARGUMENTS: (
    !counts <graphify·countValue[]>
  )
  RETURN: <string[]>
*/
function listDatasets (counts) {
  var datasetsList = [];
  _.each(counts, function (countOrCountsList) {

    // add key of each object as dataset
    if (_.isObject(countOrCountsList)) _.each(countOrCountsList, function (count, datasetLabel) { datasetsList.push(datasetLabel); })
    // add default dataset
    else datasetsList.push("default");

  });
  return _.uniq(datasetsList);
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DETERMINE DATASET COLORS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: determine backgroundColor and maybe borderColor for this dataset
  ARGUMENTS: (
    counts <graphify·countValue[]>,
    labels <string[]>,
    datasetObject,
    graphOptions,
  ){@this=graphified}
  RETURN: <void>
*/
function determineDatasetColors (counts, labels, datasetObject, graphOptions) {
  var graphified = this;

  // figure out colorMaker value
  var colorMaker = datasetObject.colorMaker || graphOptions.colorMaker || function () { return $$.random.color(1); };

  // make list of colors or single color
  if (_.isFunction(colorMaker)) datasetObject.backgroundColor = _.map(counts, function (count, index) {
    return colorMaker.call(graphified, datasetObject.data[index], index, datasetObject.data, counts, datasetObject, graphOptions, labels);
  })
  else datasetObject.backgroundColor = colorMaker;

  // if line chart and single color defined, use this color for line color
  if (
    (datasetObject.type || graphOptions.type) === "line"
    && _.isString(datasetObject.backgroundColor)
  ) datasetObject.borderColor = datasetObject.backgroundColor;

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: generate list of datasets grom the given list of counts
  ARGUMENTS: (
    !counts <graphify·countValue[]>,
    !labels <string[]>,
    ?graphOptions,
  ){@this=<graphified>}
  RETURN: <{
    data: <number[]>,
    backgroundColor: <string[]>,
  }[]>
*/
module.exports = function (counts, labels, graphOptions) {
  var graphified = this;

  // get list of all datasets this count array induces
  var datasetsList = listDatasets(counts);

  // populate datasets
  var datasets = [];
  _.each(datasetsList, function (datasetName) {

    // create dataset
    var datasetObject = {
      label: datasetName,
      xAxisID: "defaultX",
      yAxisID: "defaultY",
      fill: "none",
      borderColor: "white",
      // borderWidth:
    };

    // populate counts
    if (datasetName === "default") datasetObject.data = _.map(counts, function (count) {
      return _.isObject(count) ? count.default : count;
    })
    else datasetObject.data = _.map(counts, function (count) {
      if (_.isUndefined(count)) return 0
      else return count[datasetName] || 0;
    });

    // apply custom options of this dataset
    var customDatasetOptions = $$.getValue(graphOptions.datasetsOptions, datasetObject.label);
    if (customDatasetOptions) datasetObject = $$.defaults(datasetObject, customDatasetOptions);

    // make coloring
    determineDatasetColors.call(graphified, counts, labels, datasetObject, graphOptions);

    // make pattern
    var shouldUsePattern = graphOptions.usePatterns !== false && datasetObject.usePatterns !== false && (graphOptions.type == "pie" || graphOptions.type == "doughnut" || datasetObject.data.length <= 15);
    if (shouldUsePattern) datasetObject.backgroundColor = _.map(datasetObject.backgroundColor, function (color) {
      return patternomalify(color);
    });

    // add dataset to list of datasets
    datasets.push(datasetObject);

  });

  // return datasets array
  return datasets;

};

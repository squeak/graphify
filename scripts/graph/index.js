var _ = require("underscore");
var $$ = require("squeak");
var processCounts = require("./processCounts");
var makeDatasets = require("./datasets");
var makeAxes = require("./axes");
var displayGraph = require("../displayGraph");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CONVERT COUNTS OBJECT TO COUNTS COLLECTION
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: convert label/count pairs to a collection of labels and counts object
  ARGUMENTS: ( counts <graphify·countsObject> )
  RETURN: <graphify·countAndLabelObject>
*/
function countsObjectToCollection (counts) {
  return _.map(counts, function (count, index) {
    return {
      label: index,
      count: count,
    };
  });
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: utility to create a simple chart
  ARGUMENTS: (<graphify·graphOptions>)
  RETURN: <void>
*/
module.exports = function (graphOptions, $container) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  var graphified = this;

  if (!graphOptions) return $$.log.error("[graphify] Missing options for graph to display in :", $container)
  else if (!_.isFunction(graphOptions.dataExtraction)) return $$.log.error("[graphify] Invalid graph options, mandatory option 'dataExtraction' is missing or not a function.", graphOptions);

  //
  //                              PREPARE COLLECTION AND EVENTUALLY FILTER IT

  var collectionToGraph = _.clone(graphified.collection);
  if (graphOptions.filterEntries) collectionToGraph = _.filter(collectionToGraph, graphOptions.filterEntries);

  //
  //                              EXTRACT DATA FROM COLLECTION

  var countsAndLabels = graphOptions.dataExtraction(collectionToGraph);
  if ($$.isObjectLiteral(countsAndLabels)) countsAndLabels = countsObjectToCollection(countsAndLabels);

  //
  //                              GROUP, SORT AND FILTER COUNTS

  var countsAndLabels = processCounts(countsAndLabels, graphOptions);

  //
  //                              SEPARATE LIST OF LABELS AND COUNTS, AND GET COLORS

  // make list of labels
  var finalLabels = _.pluck(countsAndLabels, "label");
  if (graphOptions.labelDisplay) finalLabels = _.map(finalLabels, graphOptions.labelDisplay);

  // generate datasets
  var datasets = makeDatasets.call(graphified, _.pluck(countsAndLabels, "count"), finalLabels, graphOptions);

  // generate axes
  var finalAxes = makeAxes(datasets, graphOptions);

  //
  //                              CREATE CHART.JS OPTIONS

  var chartjsOptions = {
    type: graphOptions.type,
    data: {
      labels: finalLabels,
      datasets: datasets,
    },
    options: {
      _recursiveOption: true,
      scales: finalAxes,
    },
  };

  // title
  if (graphOptions.title) $$.setValue(chartjsOptions, "options.title", {
    _recursiveOption: true,
    display: true,
    text: graphOptions.title,
    fontColor: "white",
  });

  // chart click options
  if (graphOptions.chartClick) $$.setValue(chartjsOptions, "options.onClick", function (event) {
    var bars = this.getElementAtEvent(event);
    graphOptions.chartClick(bars[0], datasets, collectionToGraph, this, event);
  });

  //
  //                              MAKE FULL OPTIONS

  var finalChartjsOptions = $$.defaults(chartjsOptions, graphOptions.chartjsOptions);

  //
  //                              MAKE GRAPH

  displayGraph($container, finalChartjsOptions);

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

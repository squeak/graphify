
Graphify is a library to display some stats and graphs for a database.
It is based on [chart.js](https://www.chartjs.org/) and therefore has all it's features.
Graphify contains just some preset default values, and make it easy to display many graphs directly in a panel.

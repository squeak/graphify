# API

🛈 This API is written using squyntax notation, information/documentation about it is available [here](https://squeak.eauchat.org/libs/squyntax/). 🛈


## Options

```javascript
graphify·options = <{

  ?$container: <yquerjObject>@default=$("body") «
    if not set, will be displayed in a fullscreen dialog
    if set, you wont be able to use the "hide", "show" and "destroy" methods
  »,
  ?title: <string> « title of the graphs panel »,
  ?collection: <object> « list of entries to graph »,
  ?statsOverview: <boolean>@default=true « if true, will display some overview stats useful to all types of databases in first tab »,
  ?graphs: <graphify·graphOptions[]> « list of graphs to create »,

}>
```

Types used in graphify·options:
```javascript
// GRAPH OPTIONS
graphify·graphOptions = <{

  // graph name and title
  !name: <string> « name of this graph (title of this graph tab) »,
  ?title: <string> « title of this graphs »,

  // filter entries to extract data from
  ?filterEntries: <function(entry)> « function passed to _.filter on list of entries (before making counts) »,

  // extracting graph data
  !dataExtraction: <function(collection<entry[]>):<graphify·countAndLabelObject[]|graphify·countsObject>> « should return the list of labels and counts to graph »,

  // group, sort and filter counts
  ?groupCounts: <function(count<graphify·countAndLabelObject>)> « if you want to group counts somehow to simplify your graph, for example »,
  ?filterCounts: <function(count<graphify·countAndLabelObject>)> « function passed to _.filter on counts result »,
  ?sortCounts: <
    | "label"
    | "count"
    | function(count<graphify·countAndLabelObject>):<string|number>
  > « sort datasets by label, count or custom function »,
  ?sortReverse: <boolean> « if true will reverse sorting »,

  // graph general styles
  ?type: <"bar"|"horizontalBar"|"line"|"radar"|"pie"|"doughnut"|"polarArea"|"bubble"|"scatter">,
  ?colorMaker: <function(
    count <graphify·countValue> « the current value to display in graph »,
    index <integer> « the index of the current value in datasetCounts »,
    datasetCounts <graphify·countValue[]> « the list of all values of this dataset to display in graph »,
    counts <graphify·countValue[]> « the list of all values of this graph to display in graph »,
    datasetOptions <object> « the full options of the current dataset in which this count is displayed »,
    graphOptions <object> « the full options of the current graph »,
    labels <string[]> « the list of all labels to display in graph legend (ordered as counts array) »,
  )>,
  ?usePatterns: <boolean>@default=true « if false won't patternomalify »,
  ?labelDisplay: <function(label<string>):<string>> « customize labels display »,

  // customize datasets specifically
  ?datasetsOptions: <{
    [datasetName <string>]: <{
      ?type: <"bar"|"horizontalBar"|"line"|"radar"|"pie"|"doughnut"|"polarArea"|"bubble"|"scatter"> « type of chart you want to use for this dataset »,
      ?colorMaker: <same as upper colorMaker> « color processing to apply only to this dataset »,
      ?xAxisID: <string> « id of the x axis to use for this dataset »,
      ?yAxisID: <string> « id of the y axis to use for this dataset »,
      ?usePatterns: <boolean>@default=<true if pie, doughnut or if only few entries> « if true, will apply a pattern to the surfaces »,
    }>
  }> « custom options to add to each dataset »,

  // customize axes
  ?axesOptions: <{
    [axesId <string|"default">]: <{
      ?stacked: <boolean> « if true, will stack datasets that use this axis »,
      ?position: <"left"|"right"|"top"|"bottom"> « position of this axis in relation with the graph »,
      ... any other scale option accepted by chart.js (https://www.chartjs.org/docs/latest/axes/)
    }>,
  }>,

  // clicking chart pieces
  charClick: <function(
    element<chartjsElement>,
    datasets<chartjsDatasets>,
    collection<object[]> « collection at it was passed to be graphed »,
    chart<chartjsChart>,
    event « click event »,
  )>,

  // custom chartjs options
  ?chartjsOptions: <see chart.js documentation> « any custom options to pass to the chartjs class instanciation »,

}>

// OPTIONS USED IN GRAPH OPTIONS
graphify·valueToCount = <string|number|<string|number>[]>
graphify·countValue = <
  |-- <number> « if there is only one dataset in the graph, the value to use in graph for this entry »
  |-- <{ [datasetName]: <number> }> « if there are many datasets in the graph, the list of values to use in graph (organnized by dataset they belong to) »
>
graphify·countAndLabelObject = <{
  label: <string>,
  count: <graphify·countValue>,
}>
graphify·countsObject = <{
  [label<string>]: count <graphify·countValue>
}>
```


## Return

The following is the type of the object returned by graphify:
```javascript
graphified = {
  options: <graphify·options>,
  collection: <object[]> « the passed collection out of which to make graphs »,
  isVisible: <boolean> « specifies if the graphify panel is currently visible »,
  uuid: <string> « unique id of this graphified object »,
  refreshId: <string> « unique id of the refreshing of this graphified object (= changed each time graphified is refreshed) »,
  show: <function(ø)> « show graphify panel »,
  hide: <function(ø)> « hide graphify panel »,
  destroy: <function(ø)> « destroy graphify panel »,
  refresh: <function(ø)> « refresh graphify panel (useful if collection was modified) »,
}
```

# Usage

Load and use graphify library with `graphify(options)`.  

Or in more details:
```javascript
var myCollection = [
  { person: "Iana",    year: 1915, score: 1, },
  { person: "Michel",  year: 1658, score: 3, },
  { person: "Hector",  year: 1917, score: 3, },
  { person: "Victor",  year: 1912, score: 2, },
  { person: "Berta",   year: 2021, score: 3, },
  { person: "Simona",  year: 2021, score: 1, },
  { person: "Solomon", year: 2031, score: 3, },
];

graphify({
  $container: $("body"),
  collection: myCollection,
  statsOverview: false,
  graphs: [
    {
      name: "scores bar graph",
      type: "bar",
      dataExtraction: function (collection) { return _.countBy("score"); },
      sortCounts: "label",
    },
    {
      name: "centuries pie",
      type: "pie",
      dataExtraction: function (collection) {
        return _.countBy(function (entry) {
          return (entry.year+"").match(/^\d\d/)[0];
        });
      },
      labelDisplay: function (label) {
        return label +"th century";
      },
    },
  ],
});
```

require("squeak/extension/string");
var graphify = require("../../../scripts");
var uify = require("uify");

window.demoStart = function (utilities) {

  utilities.section({
    title: "graphify",
    demo: function ($container) {

      //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
      //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
      //                                                  SIMPLE COLLECTION
      //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

      //
      //                              COLLECTION

      var myCollection = [
        { person: "Iana",    year: 1915, score: 1, },
        { person: "Michel",  year: 1658, score: 3, },
        { person: "Hector",  year: 1917, score: 3, },
        { person: "Victor",  year: 1912, score: 2, },
        { person: "Berta",   year: 2021, score: 3, },
        { person: "Simona",  year: 2021, score: 1, },
        { person: "Solomon", year: 2031, score: 3, },
        { person: "Germain", year: 1632, score: 8, },
        { person: "Noha",    year: 1632, score: 5, },
        { person: "Paul",    year: 1432, score: 6, },
      ];
      $container.div({
        htmlSanitized: "Collection: <br>"+ $$.string.make(myCollection),
      });

      //
      //                              STATS BUTTON

      uify.button({
        $container: $container,
        icomoon: "stats-bars",
        title: "show stats",
        inlineTitle: "bottom",
        css: { fontSize: "2em", },
        click: function () {

          graphify({
            collection: myCollection,
            statsOverview: false,
            graphs: [
              {
                name: "scores bar graph",
                type: "bar",
                dataExtraction: function (collection) {
                  return _.countBy(collection, "score");
                },
                sortCounts: "label",
              },
              {
                name: "centuries pie",
                type: "pie",
                dataExtraction: function (collection) {
                  return _.countBy(collection, function (entry) {
                    return (entry.year+"").match(/^\d\d/)[0];
                  });
                },
                labelDisplay: function (label) {
                  return label +"th century";
                },
              },
            ],
          });

        },
      });

      //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
      //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
      //                                                  MORE COMPLEX RANDOMLY GENERATED COLLECTION
      //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

      uify.button({
        $container: $container,
        icomoon: "stats-dots",
        title: "graph on other dataset",
        inlineTitle: "bottom",
        css: { fontSize: "2em", },
        click: function () {

          //
          //                              GENERATE COLLECTION

          var otherCollection = [];
          for (i=0; i<1000; i++) {
            var year = $$.random.integer(1000, 1999);
            otherCollection.push({
              year: year,
              population: Math.round(((year**4)/(1000**3)) * $$.random.number(0.8, 1.2)),
              expectancy: $$.round(
                (year * $$.random.number(0.8, 1.2)) / 20,
                2
              )
            });
          };

          //
          //                              GRAPH OPTIONS MAKER

          function makeGraphOptions (opts) {
            return $$.defaults({
              type: "line",
              dataExtraction: function (collection) {

                return _.map(collection, function (entry) {
                  return {
                    label: entry.year,
                    count: {
                      population: entry.population,
                      expectancy: entry.expectancy,
                    },
                  };
                });

              },
              sortCounts: "label",
              datasetsOptions: {
                population: {
                  colorMaker: function (count, index, allCounts) {
                    return count > _.max(allCounts)/2 ? "red" : "green";
                  },
                },
                expectancy: {
                  yAxisID: "expectancyAxis",
                  colorMaker: "peachpuff",
                },
              },
              axesOptions: {
                expectancyAxis: {
                  position: "right",
                  ticks: {
                    beginAtZero: true,
                    fontColor: "peachpuff",
                  },
                },
              },
            }, opts)
          };

          //
          //                              DISPLAY GRAPHIFY

          graphify({
            collection: otherCollection,
            statsOverview: false,
            graphs: [
              makeGraphOptions({
                name: "population and expectancy by years",
              }),
              makeGraphOptions({
                name: "by decades",
                groupCounts: function (count) {
                  return Math.trunc(count.label / 10) + "0s";
                },
              }),
              makeGraphOptions({
                name: "by century",
                groupCounts: function (count) {
                  return Math.trunc(count.label / 100) + "th";
                },
              }),
            ],
          });

          //                              ¬
          //

        },
      });

    },
  });

};
